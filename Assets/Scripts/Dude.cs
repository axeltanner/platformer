﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Dude : MonoBehaviour {

	private Animator animator;
	private int playerHealth = 50;
	private bool facingRight;
	public Text healthText;
	private int health;
	private bool isStrong;
	
	protected void Start(){
		//base.Start ();
		animator = GetComponent<Animator> ();
		healthText.text = "Health: " + playerHealth;
	}

	void Update(){
		Movement ();
	}

	void Movement(){
		if(Input.GetKey(KeyCode.RightArrow)){
			transform.Translate(Vector2.right * 40f * Time.deltaTime);
			//transform.eulerAngles = new Vector2(0, 180);
			animator.SetTrigger("playerRun");
			if(!facingRight){
				Flip();
			}

		}
		if(Input.GetKey(KeyCode.LeftArrow)){
			transform.Translate(Vector2.left * 40f * Time.deltaTime);
			animator.SetTrigger("playerRun");
			if(facingRight){
				Flip();
			}
			//transform.eulerAngles = new Vector2(0, 0);
		}
		if(Input.GetKey(KeyCode.UpArrow)){
			transform.position += transform.up * 80f * Time.deltaTime;
			animator.SetTrigger("Jump");
		}
		if(Input.GetKey(KeyCode.DownArrow)){
			transform.position += transform.right * 80f * Time.deltaTime;
			animator.SetTrigger("Dude-Slide");
		}
	}

	protected void Flip(){
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}
	

	public void TakeDamage(int damageReceived){
		playerHealth -= damageReceived;
		animator.SetTrigger ("playerHurt");
		Debug.Log ("Player Health: " + playerHealth);
	}

	//protected override void HandleCollisions<T>(T component){
		//BowlingBall ball = component as BowlingBall;
		//animator.SetTrigger ("Fall");
	//}
	
	void OnTriggerEnter2D(Collider2D other)
	{

		if (other.gameObject.tag == "Bomb")
			
		{
			other.gameObject.SetActive(false);
			isStrong = true;
			Debug.Log("isStrong");
			transform.localScale += new Vector3(0, 2f, 2f);
		}

		if (other.gameObject.tag == "Duck")
			
		{
			if(isStrong == true){
				other.gameObject.SetActive(false);
				health = health -1;
				Debug.Log("AYE");
			}
		}

		if (other.gameObject.tag == "Finish")
			
		{
			Debug.Log("Finish");
		}




	}
}
