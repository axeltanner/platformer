﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

	private GameObject startImage;
	private Text startText;

	// Use this for initialization
	void Awake () {
	
	}
	
	// Update is called once per frame
	void Start () {
		startImage = GameObject.Find ("Start Image");
		startImage.SetActive (true);
		Invoke ("DisableStartImage", 3/2);
	}

	private void DisableStartImage(){
		
		startImage.SetActive (false);
		
	}

}
