﻿using UnityEngine;
using System.Collections;

public class BigDuck : MonoBehaviour {

	public float moveSpeed = -20f;
	public Vector2 moveAmount;
	private float moveDirection = -20f;
	private Animator animator;
	
	void Start () {
		animator = GetComponent<Animator> ();
	}

	void Update () {
		moveAmount.x = moveDirection * moveSpeed * Time.deltaTime;
		transform.Translate(moveAmount); //Move the enemy
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		
		if (other.gameObject.tag == "Wall")
			
		{
			Flip();	
		}
	}
	
	public void Flip()
	{
		moveDirection *= -1;
		
		
		
		// Flip the sprite by multiplying the x component of localScale by -1.
		
		Vector3 enemyScale = transform.localScale;
		enemyScale.x *= -1;
		transform.localScale = enemyScale;
	}
}
